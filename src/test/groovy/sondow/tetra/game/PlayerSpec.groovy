package sondow.tetra.game

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import sondow.tetra.io.Database
import sondow.tetra.io.Outcome
import sondow.tetra.io.TwitterPollMaker
import spock.lang.Specification
import twitter4j.Card
import twitter4j.Choice
import twitter4j.Status
import twitter4j.StatusWithCard

class PlayerSpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    private void initEnvironment() {
        envVars.set('twitter_account', 'EmojiTetraGamma')
        envVars.set('EmojiTetraGamma_twitter4j_oauth_accessToken', 'ghostpowder')
        envVars.set('EmojiTetraGamma_twitter4j_oauth_accessTokenSecret', 'auntfrenchie')
        envVars.set('CRED_AWS_ACCESS_KEY', 'lemongiant')
        envVars.set('CRED_AWS_SECRET_KEY', 'truthfulmarionette')
    }

    private String readFile(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        String filePath = classLoader.getResource(fileName).getFile();
        new File(filePath).text
    }

    private String gameStateSimple = readFile 'game-state-simple-verbose.json'
    private String gameStateExample = readFile 'game-state-example-verbose.json'

    private String initialTeeGame = '' +
            'Next ⡇　Score 0\n\n' +
            '◽◽😱😱😱◽◽\n' +
            '◽◽◽😱◽◽◽\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽'

    private List<String> startingMoves = ['⬅️ Left', '➡️ Right', '⬇️ Down', '⏬ ' +
            'Plummet']

    private List<String> standardMoves = ['⬅️ Left', '➡️ Right', '🔄 Rotate', '⬇️ Down']

    def "should start new twitter thread when database starts empty"() {
        setup:
        initEnvironment()
        Database database = Mock(Database)
        TwitterPollMaker pollMaker = Mock(TwitterPollMaker)
        Random random = new Random(4)
        Player player = new Player(database, pollMaker, random)
        Status tweet = Mock(Status)

        when:
        Outcome outcome = player.play()

        then:
        1 * database.readGameState() >> null
        1 * pollMaker.readPreviousTweet(null) >> null
        1 * pollMaker.postPoll(20, initialTeeGame, startingMoves, null) >> tweet
        1 * database.writeGameState('{"sc":0,"cur":{"r":0,"c":3,"t":"T","d":"N"},' +
                '"thm":"HALLOWEEN","nx":"I","id":0,"rows":"9*.7-2*.7","thr":1}')
        outcome.tweet == tweet
    }

    def "should continue thread when database has game state, with super majority"() {
        setup:
        initEnvironment()
        Database database = Mock(Database)
        TwitterPollMaker pollMaker = Mock(TwitterPollMaker)
        Random random = new Random(4)
        Player player = new Player(database, pollMaker, random)
        StatusWithCard previousTweet = Mock(StatusWithCard)
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 30), // Super majority
                new Choice("➡️️ Right", 0),
                new Choice("🔄 Rotate", 0),
                new Choice("⬇️ Down", 0)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        Status tweet = Mock(Status)

        when:
        Outcome outcome = player.play()

        then:
        1 * database.readGameState() >> gameStateSimple
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.postPoll(20, _, standardMoves, 1234567890L) >> tweet
        1 * tweet.getId() >> 87654L
        1 * database.writeGameState('{"sc":100,"cur":{"r":2,"c":2,"t":"J","d":"E"},"thm":"FRUIT",' +
                '"nx":"Z","id":87654,"rows":"9*.7-.7-.I4ZZ","thr":32}')
        outcome.tweet == tweet
    }

    def 'should start new thread after thread gets long'() {
        setup:
        initEnvironment()
        Database database = Mock(Database)
        TwitterPollMaker pollMaker = Mock(TwitterPollMaker)
        Random random = new Random(4)
        Player player = new Player(database, pollMaker, random)
        StatusWithCard oldGameTweet = Mock(StatusWithCard)
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 30), // Super majority
                new Choice("➡️️ Right", 0),
                new Choice("🔄 Rotate", 0),
                new Choice("⬇️ Down", 0)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        Status newGameTweet = Mock(Status)
        Status introTweet = Mock(Status)
        Status outroTweet = Mock(Status)

        when:
        player.play()

        then:
        1 * database.readGameState() >> gameStateExample
        1 * pollMaker.readPreviousTweet(987654321L) >> oldGameTweet
        2 * oldGameTweet.getCard() >> previousPoll
        2 * oldGameTweet.getId() >> 987654321L
        1 * pollMaker.tweet('Continuing game from thread… https://twitter' +
                '.com/EmojiTetraGamma/status/987654321', null) >> introTweet
        1 * introTweet.getId() >> 898989898L
        1 * pollMaker.postPoll(20, _, startingMoves, 898989898L) >> newGameTweet
        1 * pollMaker.tweet('Game continues in new thread… https://twitter' +
                '.com/EmojiTetraGamma/status/87654', 987654321L) >> outroTweet
        2 * newGameTweet.getId() >> 87654L
        1 * database.writeGameState('{"sc":1500,"cur":{"r":0,"c":2,"t":"J","d":"S"},' +
                '"thm":"FRUIT","nx":"L","id":87654,"rows":"3*.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SSOO' +
                '.LL-.TOOJ3-T3.ZZJ-.I4ZZ","thr":1}')
        0 * _._
    }

}
