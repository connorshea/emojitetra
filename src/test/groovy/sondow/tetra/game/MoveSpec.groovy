package sondow.tetra.game

import spock.lang.Specification

import static sondow.tetra.game.Move.DOWN
import static sondow.tetra.game.Move.LEFT
import static sondow.tetra.game.Move.RIGHT
import static sondow.tetra.game.Move.ROTATE
import static sondow.tetra.game.Move.STOP

class MoveSpec extends Specification {

    def "should get Move enumeration items from poll entry strings"() {
        expect:
        LEFT == Move.parse("⬅️ Left")
        LEFT == Move.parse("Left ⬅️")
        RIGHT == Move.parse("➡️ Right️")
        RIGHT == Move.parse("Right️ ➡️")
        ROTATE == Move.parse("🔄 Rotate")
        ROTATE == Move.parse("Rotate 🔄")
        DOWN == Move.parse("⬇️ Down")
        DOWN == Move.parse("Down ⬇️")
        STOP == Move.parse("⬇️ Stop")
        STOP == Move.parse("Stop ⬇️")
    }

    def "should make poll choices from moves and preserve order"() {
        when:
        String left = "⬅️ Left"
        String right = "➡️ Right"
        String rotate = "🔄 Rotate"
        String down = "⬇️ Down"
        String stop = "⬇️ Stop"

        then:
        [left] == Move.toPollChoices([LEFT])
        [right] == Move.toPollChoices([RIGHT])
        [rotate] == Move.toPollChoices([ROTATE])
        [down] == Move.toPollChoices([DOWN])
        [stop] == Move.toPollChoices([STOP])
        [left, right, rotate, down, stop] == Move.toPollChoices([LEFT, RIGHT, ROTATE, DOWN, STOP])
        [right, rotate, down] == Move.toPollChoices([RIGHT, ROTATE, DOWN])
        [left, stop] == Move.toPollChoices([LEFT, STOP])
    }

}
