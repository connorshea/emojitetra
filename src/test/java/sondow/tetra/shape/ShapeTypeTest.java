package sondow.tetra.shape;

import java.util.Random;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sondow.tetra.game.Direction;
import sondow.tetra.game.Game;
import sondow.tetra.io.EmojiSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ShapeTypeTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testFromLetter() {
        assertEquals(ShapeType.ZEE, ShapeType.fromLetter("Z"));
        assertEquals(ShapeType.LINE, ShapeType.fromLetter("I"));
        assertEquals(ShapeType.TEE, ShapeType.fromLetter("T"));
        assertEquals(ShapeType.JAY, ShapeType.fromLetter("J"));
        assertEquals(ShapeType.SQUARE, ShapeType.fromLetter("O"));
        assertEquals(ShapeType.ELL, ShapeType.fromLetter("L"));
        assertEquals(ShapeType.ESS, ShapeType.fromLetter("S"));
        Throwable t = null;
        try {
            ShapeType.fromLetter("P");
        } catch (NoSuchShapeException e) {
            t = e;
            assertEquals("No ShapeType for P", t.getMessage());
        }
        assertNotNull(t);
    }

    @Test
    public void testCreateShape() {
        Game game = new Game(EmojiSet.BOOK);

        Shape shape1 = ShapeType.createShape(ShapeType.ZEE, game, Direction.NORTH);
        Shape shape2 = ShapeType.createShape(ShapeType.LINE, game, Direction.NORTH);
        Shape shape3 = ShapeType.createShape(ShapeType.TEE, game, Direction.NORTH);
        assertEquals(ShapeZee.class, shape1.getClass());
        assertEquals(shape1.context, game);
        assertEquals(ShapeLine.class, shape2.getClass());
        assertEquals(shape2.context, game);
        assertEquals(ShapeTee.class, shape3.getClass());
        assertEquals(shape3.context, game);
    }

    @Test
    public void testCreateAny() {
        Random random = new Random(5L);
        Game game = new Game(EmojiSet.BOOK);
        Shape shape1 = ShapeType.createAny(random, game);
        Shape shape2 = ShapeType.createAny(random, game);
        Shape shape3 = ShapeType.createAny(random, game);
        Shape shape4 = ShapeType.createAny(random, game);
        assertEquals(ShapeTee.class, shape1.getClass());
        assertEquals(ShapeSquare.class, shape2.getClass());
        assertEquals(ShapeSquare.class, shape3.getClass());
        assertEquals(ShapeZee.class, shape4.getClass());
    }

    @Test
    public void testIsValidLetter() {
        assertTrue(ShapeType.isValidLetter("Z"));
        assertTrue(ShapeType.isValidLetter("I"));
        assertTrue(ShapeType.isValidLetter("T"));
        assertTrue(ShapeType.isValidLetter("J"));
        assertTrue(ShapeType.isValidLetter("O"));
        assertTrue(ShapeType.isValidLetter("L"));
        assertTrue(ShapeType.isValidLetter("S"));
        assertFalse(ShapeType.isValidLetter("."));
        assertFalse(ShapeType.isValidLetter("z"));
        assertFalse(ShapeType.isValidLetter("ZZ"));
        assertFalse(ShapeType.isValidLetter(".."));
    }

    @Test
    public void testGetBottomTileRowIndex() {
        Game game = new Game(EmojiSet.BOOK);
        Shape shapeZ = ShapeType.createShape(ShapeType.ZEE, game, Direction.NORTH);
        Shape shapeI = ShapeType.createShape(ShapeType.LINE, game, Direction.NORTH);
        Shape shapeT = ShapeType.createShape(ShapeType.TEE, game, Direction.NORTH);

        assertEquals(1, shapeZ.getBottomTileRowIndex());
        assertEquals(0, shapeI.getBottomTileRowIndex());
        assertEquals(1, shapeT.getBottomTileRowIndex());
    }
}
