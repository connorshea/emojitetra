package sondow.tetra.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sondow.tetra.io.EmojiSet;
import sondow.tetra.shape.Shape;
import sondow.tetra.shape.ShapeType;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("UnnecessaryLocalVariable")
public class GameTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    private List<Move> list(Move... moves) {
        return Arrays.asList(moves);
    }

    private List<Move> LEFT_RIGHT_ROTATE_DOWN = list(Move.LEFT, Move.RIGHT, Move.ROTATE, Move.DOWN);
    private List<Move> LEFT_RIGHT_ROTATE_STOP = list(Move.LEFT, Move.RIGHT, Move.ROTATE, Move.STOP);
    private List<Move> LT_RT_DN_PLUMMET = list(Move.LEFT, Move.RIGHT, Move.DOWN, Move.PLUMMET);
    private List<Move> RT_ROT_DN_PLUMMET = list(Move.RIGHT, Move.ROTATE, Move.DOWN, Move.PLUMMET);
    private List<Move> LT_ROT_DN_PLUMMET = list(Move.LEFT, Move.ROTATE, Move.DOWN, Move.PLUMMET);

    @Test
    public void shapeFrequenciesAreRoughlyEven() {

        Map<ShapeType, AtomicLong> shapeCounts = new HashMap<>();

        long total = 10000;
        int types = 7;
        long avg = (total / types);
        double min = avg * 0.88;
        double max = avg * 1.12;
        Set<ShapeType> shapeTypes = new TreeSet<>(Arrays.asList(ShapeType.values()));

        for (int i = 0; i < total; i++) {
            Game game = new Game(EmojiSet.AQUATIC, new Random(12));
            ShapeType shapeType = ShapeType.createAny(new Random(), game).getShapeType();
            AtomicLong count = shapeCounts.get(shapeType);
            if (count == null) {
                shapeCounts.put(shapeType, new AtomicLong(1L));
            } else {
                count.incrementAndGet();
            }
        }

        assertEquals(shapeTypes, new TreeSet<>(shapeCounts.keySet()));
        assertEquals(types, shapeCounts.keySet().size());
        for (ShapeType shapeType : shapeCounts.keySet()) {
            long count = shapeCounts.get(shapeType).get();
            String msg = "count=" + count + ", avg=" + avg + ", min=" + min + "," + " max=" + max;
            assertTrue(msg, count > min && count < max);
        }
    }

    @Test
    public void testCreateTetrisGame() {
        Game game = new Game(EmojiSet.FRUIT, new Random(7));
        Shape shape = ShapeType.createShape(ShapeType.JAY, game, Direction.NORTH);
        game.setPiece(shape);
        game.setScore(9876543);
        String expected = "" +
                "Next ⠼　Score 9876543\n\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(expected, game.toString());
    }

    @Test
    public void testNoLegalMovesWhenGameOver() {
        Game game = new Game(EmojiSet.FRUIT, new Random(7));
        Shape shape = ShapeType.createShape(ShapeType.JAY, game, Direction.NORTH);
        game.setPiece(shape);
        String expected = "" +
                "Next ⠼　Score 0\n\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(expected, game.toString());
        assertEquals(LT_RT_DN_PLUMMET, game.listLegalMoves());
        game.setGameOver(true);
        String gameOverExpected = "" +
                "Next ⠼　Score 0\n\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "⚡️⚡️⚡️⚡️⚡️⚡️⚡️\n" +
                "⚡️\uD83C\uDDEC \uD83C\uDDE6 \uD83C\uDDF2 \uD83C\uDDEA⚡️⚡️\n" +
                "⚡️⚡️\uD83C\uDDF4 \uD83C\uDDFB \uD83C\uDDEA \uD83C\uDDF7⚡️\n" +
                "⚡️⚡️⚡️⚡️⚡️⚡️⚡️\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(gameOverExpected, game.toString());
        assertEquals(new ArrayList<>(), game.listLegalMoves());
    }

    @Test
    public void testDown() {
        Game game = new Game(EmojiSet.CLOTHING, new Random(6));
        Shape shape = ShapeType.createShape(ShapeType.SQUARE, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String startExpected = "" +
                "Next ⠶　Score 0\n\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.DOWN);
        String afterOneDown = game.toString();
        String afterOneDownExpected = "" +
                "Next ⠶　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.DOWN);
        String afterTwoDownExpected = "" +
                "Next ⠶　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        String afterTwoDown = game.toString();
        game.doMove(Move.DOWN);
        String afterThreeDownExpected = "" +
                "Next ⠶　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽◽◽◽◽";
        String afterThreeDown = game.toString();
        game.doMove(Move.DOWN);
        String afterFourDown = game.toString();
        String afterFourDownExpected = "" +
                "Next ⠶　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽⬛⬛◽◽\n" +
                "◽◽◽⬛⬛◽◽";

        assertEquals(startExpected, start);
        assertEquals(afterOneDownExpected, afterOneDown);
        assertEquals(afterFourDownExpected, afterFourDown);
    }

    @Test
    public void testPlummet() {
        Game game = new Game(EmojiSet.BOOK, new Random(28));
        Shape shape = ShapeType.createShape(ShapeType.ZEE, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String startExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽📕📕◽◽◽\n" +
                "◽◽◽📕📕◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.PLUMMET);
        String afterOneDown = game.toString();
        String afterOneDownExpected = "" +
                "Next ⡇　Score 0\n\n" +
                "◽◽◽📚📚◽◽\n" +
                "◽◽📚📚◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽📕📕◽◽◽\n" +
                "◽◽◽📕📕◽◽";
        game.doMove(Move.DOWN);

        assertEquals(startExpected, start);
        assertEquals(afterOneDownExpected, afterOneDown);
    }

    @Test
    public void testLeft() {
        Game game = new Game(EmojiSet.BOOK, new Random(47));
        Shape shape = ShapeType.createShape(ShapeType.ESS, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String startExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽📚📚◽◽\n" +
                "◽◽📚📚◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LT_RT_DN_PLUMMET, game.listLegalMoves());
        game.doMove(Move.LEFT);
        String afterOneLeft = game.toString();
        String afterOneLeftExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽📚📚◽◽◽\n" +
                "◽📚📚◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LT_RT_DN_PLUMMET, game.listLegalMoves());
        game.doMove(Move.LEFT);
        String afterTwoLeft = game.toString();
        String afterTwoLeftExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽📚📚◽◽◽◽\n" +
                "📚📚◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(Arrays.asList(Move.RIGHT, Move.DOWN, Move.PLUMMET), game.listLegalMoves());
        assertEquals(startExpected, start);
        assertEquals(afterOneLeftExpected, afterOneLeft);
        assertEquals(afterTwoLeftExpected, afterTwoLeft);
    }

    @Test
    public void testRight() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(12));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String startExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽🐡🐡🐡🐡◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LT_RT_DN_PLUMMET, game.listLegalMoves());
        game.doMove(Move.RIGHT);
        String afterOneRight = game.toString();
        String afterOneRightExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽🐡🐡🐡🐡\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(Arrays.asList(Move.LEFT, Move.DOWN, Move.PLUMMET), game.listLegalMoves());
        assertEquals(startExpected, start);
        assertEquals(afterOneRightExpected, afterOneRight);
    }

    @Test
    public void testRotateEssQuarce() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.ESS, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽🐼🐼◽◽\n" +
                "◽◽🐼🐼◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LT_RT_DN_PLUMMET, game.listLegalMoves());
        game.doMove(Move.DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐼🐼◽◽\n" +
                "◽◽🐼🐼◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";

        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game
                .listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐼◽◽◽\n" +
                "◽◽◽🐼🐼◽◽\n" +
                "◽◽◽◽🐼◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = afterDownExpected;
        game.doMove(Move.ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = afterDownOneRotateOneExpected;
        game.doMove(Move.ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateSquareQuarce() {
        Game game = new Game(EmojiSet.HEART, new Random(78));
        Shape shape = ShapeType.createShape(ShapeType.SQUARE, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽💙💙◽◽\n" +
                "◽◽◽💙💙◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽💙💙◽◽\n" +
                "◽◽◽💙💙◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = afterDownExpected;
        game.doMove(Move.ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = afterDownExpected;
        game.doMove(Move.ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = afterDownExpected;
        game.doMove(Move.ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateZeeQuarce() {
        Game game = new Game(EmojiSet.CLOTHING, new Random(603));
        Shape shape = ShapeType.createShape(ShapeType.ZEE, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⡇　Score 0\n\n" +
                "◽◽👚👚◽◽◽\n" +
                "◽◽◽👚👚◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LT_RT_DN_PLUMMET, game.listLegalMoves());
        game.doMove(Move.DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "Next ⡇　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽👚👚◽◽◽\n" +
                "◽◽◽👚👚◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "Next ⡇　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽👚◽◽◽\n" +
                "◽◽👚👚◽◽◽\n" +
                "◽◽👚◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = afterDownExpected;
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = afterDownOneRotateOneExpected;
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateTeeQuarce() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(18));
        Shape shape = ShapeType.createShape(ShapeType.TEE, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽🐵🐵🐵◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LT_RT_DN_PLUMMET, game.listLegalMoves());
        game.doMove(Move.DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐵🐵🐵◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽◽🐵🐵◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽🐵🐵🐵◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽🐵🐵◽◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateEllQuarce() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(12));
        Shape shape = ShapeType.createShape(ShapeType.ELL, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽🐳🐳🐳◽◽\n" +
                "◽◽🐳◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LT_RT_DN_PLUMMET, game.listLegalMoves());
        game.doMove(Move.DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐳🐳🐳◽◽\n" +
                "◽◽🐳◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐳◽◽◽◽\n" +
                "◽◽🐳◽◽◽◽\n" +
                "◽◽🐳🐳◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽🐳◽◽\n" +
                "◽◽🐳🐳🐳◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(Move.ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐳🐳◽◽◽\n" +
                "◽◽◽🐳◽◽◽\n" +
                "◽◽◽🐳◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateJayQuarce() {
        Game game = new Game(EmojiSet.FRUIT, new Random(7));
        Shape shape = ShapeType.createShape(ShapeType.JAY, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠼　Score 0\n\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "Next ⠼　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "Next ⠼　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🍏🍏◽◽◽\n" +
                "◽◽🍏◽◽◽◽\n" +
                "◽◽🍏◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = "" +
                "Next ⠼　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🍏◽◽◽◽\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = "" +
                "Next ⠼　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🍏◽◽◽\n" +
                "◽◽◽🍏◽◽◽\n" +
                "◽◽🍏🍏◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateLineQuarce() {
        Game game = new Game(EmojiSet.CLOTHING, new Random(6));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠶　Score 0\n\n" +
                "◽◽👘👘👘👘◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "Next ⠶　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽👘👘👘👘◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.DOWN);
        String afterDownTwo = game.toString();
        String afterDownTwoExpected = "" +
                "Next ⠶　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽👘👘👘👘◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.ROTATE);
        String afterDownTwoRotateOne = game.toString();
        String afterDownTwoRotateOneExpected = "" +
                "Next ⠶　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽👘◽◽◽\n" +
                "◽◽◽👘◽◽◽\n" +
                "◽◽◽👘◽◽◽\n" +
                "◽◽◽👘◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(Move.ROTATE);
        String afterDownTwoRotateTwo = game.toString();
        String afterDownTwoRotateTwoExpected = afterDownTwoExpected;
        game.doMove(Move.ROTATE);
        String afterDownTwoRotateThree = game.toString();
        String afterDownTwoRotateThreeExpected = afterDownTwoRotateOneExpected;
        game.doMove(Move.ROTATE);
        String afterDownTwoRotateFour = game.toString();
        String afterDownTwoRotateFourExpected = afterDownTwoExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownTwoExpected, afterDownTwo);
        assertEquals(afterDownTwoRotateOneExpected, afterDownTwoRotateOne);
        assertEquals(afterDownTwoRotateTwoExpected, afterDownTwoRotateTwo);
        assertEquals(afterDownTwoRotateThreeExpected, afterDownTwoRotateThree);
        assertEquals(afterDownTwoRotateFourExpected, afterDownTwoRotateFour);
    }

    @Test
    public void testGetLegalMovesCantGoLeftOffGrid() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.NORTH, 3, 1);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "🐯🐯🐯🐯◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";

        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(RT_ROT_DN_PLUMMET, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCantRotateAtTop() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(3));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠧　Score 0\n\n" +
                "◽◽🐯🐯🐯🐯◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";

        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LT_RT_DN_PLUMMET, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testSquaresCantRotateYo() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(3));
        Shape shape = ShapeType.createShape(ShapeType.SQUARE, game, Direction.NORTH);
        game.setPiece(shape);
        game.doMove(Move.DOWN);
        String gameString = game.toString();
        String expectedGameString = "" +
                "Next ⠧　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐻🐻◽◽\n" +
                "◽◽◽🐻🐻◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";

        List<Move> moves = game.listLegalMoves();
        assertEquals(expectedGameString, gameString);
        assertEquals(LT_RT_DN_PLUMMET, moves);
    }

    @Test
    public void testGetLegalMovesCantRotateTooCloseToTop() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(29));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.NORTH, 1, 3);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯🐯🐯🐯◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LT_RT_DN_PLUMMET, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCanRotateWhenNotTooCloseToTop() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.NORTH, 2, 3);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯🐯🐯🐯◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCantGoRightOffGrid() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.NORTH, 3, 4);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐯🐯🐯🐯\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LT_ROT_DN_PLUMMET, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCantRotateOffGrid() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.EAST, 3, 6);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽🐯\n" +
                "◽◽◽◽◽◽🐯\n" +
                "◽◽◽◽◽◽🐯\n" +
                "◽◽◽◽◽◽🐯\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(Arrays.asList(Move.LEFT, Move.DOWN, Move.PLUMMET), moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCantRotateOffGridWhileNearButNotTouchingEdge() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.EAST, 3, 5);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽🐯◽\n" +
                "◽◽◽◽◽🐯◽\n" +
                "◽◽◽◽◽🐯◽\n" +
                "◽◽◽◽◽🐯◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LT_RT_DN_PLUMMET, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCanRotateWhenNotLeavingGrid() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.EAST, 3, 1);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽🐯◽◽◽◽◽\n" +
                "◽🐯◽◽◽◽◽\n" +
                "◽🐯◽◽◽◽◽\n" +
                "◽🐯◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCantGoLeftWhenBlockedByTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.EAST, 7, 3);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.ELL, game), 8, 2);
        game.addTile(new Tile(ShapeType.ELL, game), 9, 2);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 2);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 3);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐯◽◽◽\n" +
                "◽◽◽🐯◽◽◽\n" +
                "◽◽◽🐯◽◽◽\n" +
                "◽◽🐨🐯◽◽◽\n" +
                "◽◽🐨◽◽◽◽\n" +
                "◽◽🐨🐨◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(RT_ROT_DN_PLUMMET, moves);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testGetLegalMovesCantGoRightWhenBlockedByTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.EAST, 7, 2);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.ELL, game), 8, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 9, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 4);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯🐨◽◽◽\n" +
                "◽◽◽🐨◽◽◽\n" +
                "◽◽◽🐨🐨◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(LT_ROT_DN_PLUMMET, moves);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testGetLegalMovesCantRotateWhenBlockedByTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.EAST, 8, 2);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.SQUARE, game), 8, 4);
        game.addTile(new Tile(ShapeType.SQUARE, game), 8, 5);
        game.addTile(new Tile(ShapeType.SQUARE, game), 9, 4);
        game.addTile(new Tile(ShapeType.SQUARE, game), 9, 5);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 4);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 5);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽🐻🐻◽\n" +
                "◽◽🐯◽🐻🐻◽\n" +
                "◽◽◽◽🐨🐨◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(LT_RT_DN_PLUMMET, moves);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testGetLegalMovesCantGoDownButCanStopWhenTouchingTopOfTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(3));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.EAST, 8, 2);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.SQUARE, game), 10, 2);
        game.addTile(new Tile(ShapeType.SQUARE, game), 10, 3);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠧　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽🐻🐻◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(Arrays.asList(Move.LEFT, Move.RIGHT, Move.ROTATE, Move.STOP), moves);
        assertEquals(expectedStart, start);

        game.doMove(Move.STOP);
        String expectedAfterStop = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽🐨🐨🐨◽◽\n" +
                "◽◽🐨◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐻🐻◽◽◽";
        assertEquals(expectedAfterStop, game.toString());
        game.doMove(Move.DOWN);
        String expectedAfterSpawnAndDown = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐨🐨🐨◽◽\n" +
                "◽◽🐨◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐻🐻◽◽◽";
        assertEquals(expectedAfterSpawnAndDown, game.toString());
    }

    @Test
    public void testStopAtBottomOfGrid() {

        Game game = new Game(EmojiSet.ANIMAL, new Random(61));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.EAST, 8, 2);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠧　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";

        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        assertEquals(expectedStart, start);

        game.doMove(Move.DOWN);
        String afterDown = "" +
                "Next ⠧　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽";
        assertEquals(afterDown, game.toString());

        assertEquals(LEFT_RIGHT_ROTATE_STOP, game.listLegalMoves());
        game.doMove(Move.STOP);
        String afterStop = "" +
                "Next ⠳　Score 0\n\n" +
                "◽◽🐨🐨🐨◽◽\n" +
                "◽◽🐨◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽";

        assertEquals(afterStop, game.toString());
    }

    @Test
    public void testDeleteRows() {

        Game game = new Game(EmojiSet.AQUATIC, new Random(9));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.EAST, 4, 4);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.LINE, game), 4, 3);
        game.addTile(new Tile(ShapeType.LINE, game), 5, 3);
        game.addTile(new Tile(ShapeType.LINE, game), 6, 3);
        game.addTile(new Tile(ShapeType.LINE, game), 7, 3);
        game.addTile(new Tile(ShapeType.JAY, game), 6, 1);
        game.addTile(new Tile(ShapeType.JAY, game), 6, 2);
        game.addTile(new Tile(ShapeType.JAY, game), 7, 2);
        game.addTile(new Tile(ShapeType.JAY, game), 8, 2);
        game.addTile(new Tile(ShapeType.SQUARE, game), 6, 5);
        game.addTile(new Tile(ShapeType.SQUARE, game), 6, 6);
        game.addTile(new Tile(ShapeType.SQUARE, game), 7, 5);
        game.addTile(new Tile(ShapeType.SQUARE, game), 7, 6);
        game.addTile(new Tile(ShapeType.SQUARE, game), 7, 0);
        game.addTile(new Tile(ShapeType.SQUARE, game), 7, 1);
        game.addTile(new Tile(ShapeType.SQUARE, game), 8, 0);
        game.addTile(new Tile(ShapeType.SQUARE, game), 8, 1);
        game.addTile(new Tile(ShapeType.TEE, game), 8, 3);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 2);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 3);
        game.addTile(new Tile(ShapeType.TEE, game), 10, 3);
        game.addTile(new Tile(ShapeType.TEE, game), 8, 5);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 5);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 6);
        game.addTile(new Tile(ShapeType.TEE, game), 10, 5);
        game.addTile(new Tile(ShapeType.ZEE, game), 9, 0);
        game.addTile(new Tile(ShapeType.ZEE, game), 9, 1);
        game.addTile(new Tile(ShapeType.ZEE, game), 10, 1);
        game.addTile(new Tile(ShapeType.ZEE, game), 10, 2);
        game.addTile(new Tile(ShapeType.ZEE, game), 10, 2);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 0);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 6);

        String expectedStart = "" +
                "Next ⠞　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽🐡◽◽\n" +
                "◽◽◽◽🐡◽◽\n" +
                "◽◽◽🐡🐡◽◽\n" +
                "◽◽◽🐡🐡◽◽\n" +
                "◽🐬🐬🐡◽🐟🐟\n" +
                "🐟🐟🐬🐡◽🐟🐟\n" +
                "🐟🐟🐬🐠◽🐠◽\n" +
                "🐙🐙🐠🐠◽🐠🐠\n" +
                "🐳🐙🐙🐠◽🐠🐳";

        assertEquals(Arrays.asList(Move.RIGHT, Move.DOWN, Move.PLUMMET), game.listLegalMoves());
        assertEquals(expectedStart, game.toString());

        game.doMove(Move.DOWN);
        String afterDown = "" +
                "Next ⡇　Score 525\n\n" +
                "◽◽🐙🐙◽◽◽\n" +
                "◽◽◽🐙🐙◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐡◽◽◽\n" +
                "◽◽◽🐡◽◽◽\n" +
                "◽🐬🐬🐡◽🐟🐟\n" +
                "🐟🐟🐬🐠🐡🐠◽";
        assertEquals(afterDown, game.toString());
    }

    @Test
    public void testDeleteNegativeOneRows() {
        Game game = new Game(EmojiSet.AQUATIC);
        try {
            game.scoreDeletedRows(-1);
            fail("Expected RuntimeException");
        } catch (RuntimeException e) {
            assertEquals("Can't delete -1 rows", e.getMessage());
        }
    }

    @Test
    public void testDeleteZeroRows() {
        Game game = new Game(EmojiSet.AQUATIC).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(0);
        assertEquals(400, game.getScore());
    }

    @Test
    public void testDeleteOneRow() {
        Game game = new Game(EmojiSet.AQUATIC).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(1);
        assertEquals(500, game.getScore());
    }

    @Test
    public void testDeleteTwoRows() {
        Game game = new Game(EmojiSet.AQUATIC).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(2);
        assertEquals(650, game.getScore());
    }

    @Test
    public void testDeleteThreeRows() {
        Game game = new Game(EmojiSet.AQUATIC).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(3);
        assertEquals(925, game.getScore());
    }

    @Test
    public void testDeleteFourRows() {
        Game game = new Game(EmojiSet.AQUATIC);
        game.setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(4);
        assertEquals(1400, game.getScore());
    }

    @Test
    public void testDeleteFiveRows() {
        Game game = new Game(EmojiSet.AQUATIC).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(5);
        assertEquals(1650, game.getScore());
    }

    @Test
    public void testCountBlankRowsBelowPieceWithTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.SOUTH, 2, 2);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.ELL, game), 8, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 9, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 4);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽🐯🐯🐯🐯◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐨◽◽◽\n" +
                "◽◽◽🐨◽◽◽\n" +
                "◽◽◽🐨🐨◽◽";
        int blankRowCount = game.countBlankRowsBelowPiece();

        assertEquals(5, blankRowCount);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testCountBlankRowsBelowPieceNoTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.SOUTH, 2, 2);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽🐯🐯🐯🐯◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        int blankRowCount = game.countBlankRowsBelowPiece();

        assertEquals(8, blankRowCount);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testCountBlankRowsBelowBottomRow() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, Direction.SOUTH, 10, 2);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "Next ⠗　Score 0\n\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽⬛⬛⬛⬛◽◽";
        int blankRowCount = game.countBlankRowsBelowPiece();

        assertEquals(0, blankRowCount);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testHalfRoundedUpMinimumOne() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        assertEquals(1, game.halfRoundedUpMinimumOne(0));
        assertEquals(1, game.halfRoundedUpMinimumOne(1));
        assertEquals(1, game.halfRoundedUpMinimumOne(2));
        assertEquals(2, game.halfRoundedUpMinimumOne(3));
        assertEquals(2, game.halfRoundedUpMinimumOne(4));
        assertEquals(3, game.halfRoundedUpMinimumOne(5));
        assertEquals(3, game.halfRoundedUpMinimumOne(6));
        assertEquals(4, game.halfRoundedUpMinimumOne(7));
        assertEquals(4, game.halfRoundedUpMinimumOne(8));
        assertEquals(5, game.halfRoundedUpMinimumOne(9));
        assertEquals(5, game.halfRoundedUpMinimumOne(10));
        assertEquals(6, game.halfRoundedUpMinimumOne(11));
        assertEquals(6, game.halfRoundedUpMinimumOne(12));
    }
}
