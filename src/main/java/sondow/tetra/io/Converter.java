package sondow.tetra.io;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import sondow.tetra.game.Direction;
import sondow.tetra.game.Game;
import sondow.tetra.game.Tile;
import sondow.tetra.shape.Shape;
import sondow.tetra.shape.ShapeType;
import twitter4j.JSONArray;
import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 * Converts stuff into other stuff, mainly json to tetra game and back.
 *
 * @author @JoeSondow
 */
public class Converter {

    public Game makeGameFromJson(String json) {
        try {
            return parse(json);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private Game parse(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        return obj.has("emojiSet") ? parseVerboseJson(obj) : parseConciseJson(obj);
    }

    private Game parseConciseJson(JSONObject obj) throws JSONException {
        String themeName = obj.getString("thm");
        EmojiSet emojiSet = EmojiSet.valueOf(themeName);
        Game game = new Game(emojiSet);
        String compressedRows = obj.getString("rows");
        List<String> expandedEncodedRows = expandEncodedRows(compressedRows);

        for (int r = 0; r < expandedEncodedRows.size(); r++) {
            String rowLatin = expandedEncodedRows.get(r);
            // Iterate through the characters of the string. Skip anything that isn't a known shape.
            for (int c = 0; c < rowLatin.length(); c++) {
                String typeLetter = "" + rowLatin.charAt(c);
                if (ShapeType.isValidLetter(typeLetter)) {
                    ShapeType shapeType = ShapeType.fromLetter("" + typeLetter);
                    Tile tile = new Tile(shapeType, game);
                    tile.setCol(c);
                    tile.setRow(r);
                    game.addTile(tile, r, c);
                }
            }
        }
        JSONObject currentPieceJson = obj.getJSONObject("cur");
        String currentTypeLetter = currentPieceJson.getString("t");
        int currentCol = currentPieceJson.getInt("c");
        int currentRow = currentPieceJson.getInt("r");
        String directionLetter = currentPieceJson.getString("d");
        Direction direction = Direction.fromInitial(directionLetter);
        ShapeType shapeType = ShapeType.fromLetter(currentTypeLetter);
        Shape shape = ShapeType.createShape(shapeType, game, direction, currentRow, currentCol);
        int score = obj.getInt("sc");
        long tweetId = obj.getLong("id");
        String nextLatinChar = obj.getString("nx");
        ShapeType nextShapeType = ShapeType.fromLetter(nextLatinChar);
        int threadLength = obj.getInt("thr");
        game.setPiece(shape).setScore(score).setNextShape(nextShapeType).setTweetId(tweetId);
        game.setThreadLength(threadLength);

        return game;
    }

    List<String> expandEncodedRows(String compressedRowsHyphenDelimited) {
        String[] compressedRowStrings = compressedRowsHyphenDelimited.split("-");
        List<String> latinRows = new ArrayList<>();
        for (String compressedSegment : compressedRowStrings) {
            char first = compressedSegment.charAt(0);
            char second = compressedSegment.charAt(1);
            String encodedRow;
            int times = 1;
            if (Character.isDigit(first) && second == '*') {
                times = Character.getNumericValue(first);
                encodedRow = compressedSegment.substring(2);
            } else {
                encodedRow = compressedSegment;
            }
            String expanded = expandRow(compressedRowsHyphenDelimited, encodedRow);
            for (int t = 0; t < times; t++) {
                latinRows.add(expanded);
            }
        }
        return latinRows;
    }

    private String expandRow(String compressedRowsHyphenDelimited, String compressedSegment) {
        // Iterate through the characters of the string.
        // If you encounter a digit, use that many copies of the previous character.
        StringBuilder builder = new StringBuilder();
        for (int c = 0; c < compressedSegment.length(); c++) {
            char ch = compressedSegment.charAt(c);
            if (Character.isDigit(ch)) {
                int count = Character.getNumericValue(ch);
                failIfDigitIsFirst(c, compressedSegment, compressedRowsHyphenDelimited);
                char previous = compressedSegment.charAt(c - 1);
                for (int i = 0; i < count - 1; i++) {
                    builder.append(previous);
                }
            } else {
                builder.append(ch);
            }
        }
        return builder.toString();
    }

    private void failIfDigitIsFirst(int c, String compressedRow, String compressedRowsHyphenDelim) {
        if (c <= 0) {
            ParseException parseException = new ParseException("Digit found at start " +
                    "of compressed row " + compressedRow + " in game data " +
                    compressedRowsHyphenDelim, 0);
            throw new RuntimeException(parseException);
        }
    }

    private Game parseVerboseJson(JSONObject obj) throws JSONException {
        String emojiSetName = obj.getString("emojiSet");
        EmojiSet emojiSet = EmojiSet.valueOf(emojiSetName);
        Game game = new Game(emojiSet);
        JSONArray rowsJson = obj.getJSONArray("rows");
        for (int r = 0; r < rowsJson.length(); r++) {
            String rowLatin = rowsJson.getString(r);
            // Iterate through the characters of the string. Skip anything that isn't a known shape.
            for (int c = 0; c < rowLatin.length(); c++) {
                String typeLetter = "" + rowLatin.charAt(c);
                if (ShapeType.isValidLetter(typeLetter)) {
                    ShapeType shapeType = ShapeType.fromLetter("" + typeLetter);
                    Tile tile = new Tile(shapeType, game);
                    tile.setCol(c);
                    tile.setRow(r);
                    game.addTile(tile, r, c);
                }
            }
        }
        JSONObject currentPieceJson = obj.getJSONObject("current");
        String currentTypeLetter = currentPieceJson.getString("type");
        int currentCol = currentPieceJson.getInt("col");
        int currentRow = currentPieceJson.getInt("row");
        String directionLetter = currentPieceJson.getString("direction");
        Direction direction = Direction.fromInitial(directionLetter);
        ShapeType shapeType = ShapeType.fromLetter(currentTypeLetter);
        Shape shape = ShapeType.createShape(shapeType, game, direction, currentRow, currentCol);
        int score = obj.getInt("score");
        long tweetId = obj.getLong("tweetId");
        String nextLatinChar = obj.getString("next");
        ShapeType nextShapeType = ShapeType.fromLetter(nextLatinChar);
        int threadLength = obj.getInt("threadLength");
        game.setPiece(shape).setScore(score).setNextShape(nextShapeType).setTweetId(tweetId);
        game.setThreadLength(threadLength);
        if (obj.has("tweetsSinceSetChange")) {
            int tweetsSinceSetChange = obj.getInt("tweetsSinceSetChange");
            game.setTweetsSinceSetChange(tweetsSinceSetChange);
        }

        return game;
    }

    public String makeJsonFromGame(Game game) {

        Map<String, Object> currentPieceMap = new HashMap<>();
        Shape current = game.getPiece();
        String letter = current.getShapeType().getLetterRepresentation();
        currentPieceMap.put("t", letter);
        currentPieceMap.put("c", current.getColumnIndex());
        currentPieceMap.put("r", current.getRowIndex());
        currentPieceMap.put("d", current.getDirection().initial);

        List<List<Tile>> rows = game.getRows();
        List<String> asciiRows = new ArrayList<>();
        for (List<Tile> row : rows) {
            StringBuilder asciiRow = new StringBuilder();
            for (Tile tile : row) {
                String representation;
                if (tile == null) {
                    representation = ".";
                } else {
                    representation = tile.getShapeType().getLetterRepresentation();
                }
                asciiRow.append(representation);
            }
            asciiRows.add(asciiRow.toString());
        }
        String compressedRows = compressAsciiRows(asciiRows);
        Map<String, Object> gameMap = new HashMap<>();
        gameMap.put("thm", game.getEmojiSet().name());
        gameMap.put("rows", compressedRows);
        gameMap.put("cur", currentPieceMap);
        gameMap.put("nx", game.getNextShape().getLetterRepresentation());
        gameMap.put("sc", game.getScore());
        gameMap.put("thr", game.getThreadLength());
        gameMap.put("id", game.getTweetId());

        return new JSONObject(gameMap).toString();
    }

    String compressAsciiRows(List<String> asciiRows) {
        List<String> compressedRows = new ArrayList<>();
        for (String asciiRow : asciiRows) {
            StringBuilder rowBuilder = new StringBuilder();
            // Iterate through the characters of the row. If a character matches the one before it,
            // increment the number of consecutive times that character should be included. When you
            // find a different character (or reach 9) add the character summary to the rowBuilder.
            // ....... becomes .7
            // ...LLL. becomes .3L3.
            // ..TTTLL becomes ..T3LL
            // ........... becomes .9..
            int times = 1;
            Character prev = null;
            for (int c = 0; c < asciiRow.length(); c++) {
                char cur = asciiRow.charAt(c);
                if (c == 0) {
                    rowBuilder.append(cur);
                } else {
                    prev = asciiRow.charAt(c - 1);
                    if (prev == cur && times < 9) {
                        times++;
                    } else {
                        appendFinalCharacterForStreak(rowBuilder, times, prev);
                        rowBuilder.append(cur);
                        times = 1;
                    }
                }
            }
            if (prev != null) {
                appendFinalCharacterForStreak(rowBuilder, times, prev);
            }
            compressedRows.add(rowBuilder.toString());
        }
        // Compress identical consecutive rows.
        // .7
        // .7
        // .7
        // ..TTLL.
        //
        // becomes
        //
        // 3*.7
        // ..TTLL.
        List<String> stage2 = new ArrayList<>();
        int times = 1;
        String curRow = null;
        for (int k = 0; k < compressedRows.size(); k++) {
            curRow = compressedRows.get(k);
            if (k >= 1) {
                String prevRow = compressedRows.get(k - 1);
                if (prevRow.equals(curRow) && times < 9) {
                    times++;
                } else {
                    stage2.add(times >= 2 ? times + "*" + prevRow : prevRow);
                    times = 1;
                }
            }
        }
        if (curRow != null) {
            stage2.add(times >= 2 ? times + "*" + curRow : curRow);
        }
        return String.join("-", stage2);
    }

    private void appendFinalCharacterForStreak(StringBuilder rowBuilder, int times, char prev) {
        if (times == 2) {
            rowBuilder.append(prev);
        } else if (times > 2) {
            rowBuilder.append(times);
        }
    }
}
