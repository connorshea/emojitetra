package sondow.tetra.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import sondow.tetra.shape.ShapeType;

public enum EmojiSet {

    ANIMAL("🐾", "🐷", "🐯", "🐵", "🐱", "🐻", "🐨", "🐼"),
    AQUATIC("🐚", "🐙", "🐡", "🐠", "🐬", "🐟", "🐳", "🐌"),
    BOOK("📖", "📕", "📙", "📒", "📗", "📘", "📔", "📚"),
    CLOTHING("👙", "👚", "👘", "👔", "👗", "🎽", "👕", "👖"),
    FRUIT("💩", "🍎", "🍑", "🍋", "🍏", "🍇", "🍉", "🍓"),
    HALLOWEEN("💀", "👺", "🎃", "😱", "🍭", "🔮", "😈", "🍫"),
    HEART("💋", "💘", "💖", "💛", "💚", "💙", "💜", "💗"),
    PLANT("🍂", "🌸", "🌻", "🌼", "🍀", "🌿", "🌺", "🍄");

    final String gone;
    final String zee;
    final String line;
    final String tee;
    final String jay;
    final String square;
    final String ell;
    final String ess;

    EmojiSet(String gone, String zee, String line, String tee, String jay, String square, String
            ell, String ess) {
        this.gone = gone;
        this.zee = zee;
        this.line = line;
        this.tee = tee;
        this.jay = jay;
        this.square = square;
        this.ell = ell;
        this.ess = ess;
    }

    public static final String blank = "◽";

    public String showcase() {
        return zee + line + tee + jay + square + ell + ess;
    }

    public static EmojiSet basedOnShowcase(String showcase) {
        EmojiSet[] allSets = values();
        EmojiSet choice = null;
        for (EmojiSet emojiSet : allSets) {
            if (showcase.equals(emojiSet.showcase())) {
                choice = emojiSet;
                break;
            }
        }
        return choice;
    }

    public static List<EmojiSet> pickAny(int count, Random random) {
        List<EmojiSet> selection = new ArrayList<>();
        EmojiSet[] allSets = values();
        for (int i = 0; i < count; i++) {
            while (selection.size() < count) {
                int choiceIndex = random.nextInt(allSets.length);
                EmojiSet emojiSet = allSets[choiceIndex];
                if (!selection.contains(emojiSet)) {
                    selection.add(emojiSet);
                }
            }
        }
        return selection;
    }

    public String symbolFor(ShapeType shapeType) {
        String symbol = null;
        if (shapeType.equals(ShapeType.ZEE)) {
            symbol = zee;
        } else if (shapeType.equals(ShapeType.LINE)) {
            symbol = line;
        } else if (shapeType.equals(ShapeType.TEE)) {
            symbol = tee;
        } else if (shapeType.equals(ShapeType.JAY)) {
            symbol = jay;
        } else if (shapeType.equals(ShapeType.SQUARE)) {
            symbol = square;
        } else if (shapeType.equals(ShapeType.ELL)) {
            symbol = ell;
        } else if (shapeType.equals(ShapeType.ESS)) {
            symbol = ess;
        }
        return symbol;
    }

    public static EmojiSet pickOne(Random random) {
        List<EmojiSet> sets = Arrays.asList(EmojiSet.PLANT, EmojiSet.CLOTHING, EmojiSet.HALLOWEEN);
        return sets.get(random.nextInt(sets.size()));
    }

    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            System.out.println(EmojiSet.pickAny(4, new Random()));
        }

        String aquaticShowcase = EmojiSet.AQUATIC.showcase();
        System.out.println(aquaticShowcase);
        System.out.println(EmojiSet.basedOnShowcase(aquaticShowcase) == EmojiSet.AQUATIC);
    }
}
