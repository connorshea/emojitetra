package sondow.tetra.conf;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import twitter4j.conf.Configuration;

public class TetraConfig implements AWSCredentialsProvider {

    /**
     * Consumer key for "Twitter for iPhone" a widely known and used app key that is one of the few
     * apps permitted to post and read twitter polls, as of 2018.
     */
    public static final String CONSUMER_KEY = "IQKbtAYlXLripLGPWd0HUA";

    /**
     * Consumer Secret for "Twitter for iPhone" a widely known and used app key that is one of the
     * few apps permitted to post and read twitter polls, as of 2018.
     */
    public static final String CONSUMER_SECRET = "GgDYlkSvaPxGxC4X8liwpUoqKwwr3lCADbz8A7ADU";

    /**
     * The configuration for Twitter.
     */
    private Configuration twitterConfig;

    /**
     * The configuration for Amazon Web Services (AWS).
     */
    private final AWSCredentials awsCredentials;

    /**
     * The number of minutes between turns, as specified by the TURN_LENGTH_MINUTES environment
     * variable which takes precedence over the length specified in the database, or the default of
     * 20.
     */
    private Integer turnLengthMinutes;

    TetraConfig(Configuration twitterConfig, AWSCredentials awsCredentials, Integer
            turnLengthMinutes) {

        this.twitterConfig = twitterConfig;
        this.awsCredentials = awsCredentials;
        this.turnLengthMinutes = turnLengthMinutes;
    }

    public Configuration getTwitterConfig() {
        return twitterConfig;
    }

    @Override
    public AWSCredentials getCredentials() {
        return awsCredentials;
    }

    public Integer getTurnLengthMinutes() {
        return turnLengthMinutes;
    }

    @Override
    public void refresh() {
    }
}
