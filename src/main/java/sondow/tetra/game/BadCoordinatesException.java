package sondow.tetra.game;

/**
 * This exception gets thrown any time a tile gets sent to a position that is illegal, either
 * because the position is off the visible game grid, or because another tile is already at that
 * position.
 *
 * @author @JoeSondow
 */
@SuppressWarnings("serial")
public class BadCoordinatesException extends RuntimeException {
    /**
     * Creates a new <code>BadCoordinatesException</code> with no details.
     */
    public BadCoordinatesException() {
        super("Invalid coordinates");
    }

    /**
     * Creates a new <code>BadCoordinatesException</code> with specified coordinates of the tile
     * space where the problem or collision occurred.
     *
     * @param col the column index of the tile space
     * @param row the row index of the tile space
     */
    public BadCoordinatesException(int col, int row) {
        super("Invalid coordinates: col=" + col + ", row=" + row);
    }
}
