package sondow.tetra.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Move {
    LEFT("⬅️"), RIGHT("➡️"), ROTATE("🔄"), DOWN("⬇️"), STOP("⬇️"), PLUMMET("⏬");

    private final String emoji;

    static List<Move> LEFT_RIGHT_ROTATE_DOWN = Arrays.asList(LEFT, RIGHT, ROTATE, DOWN);

    Move(String emoji) {
        this.emoji = emoji;
    }

    String emojiAndTitleCase() {
        String lower = this.name().toLowerCase();
        return emoji + " " + Character.toUpperCase(lower.charAt(0)) + lower.substring(1);
    }

    /**
     * Tries to find a matching Move for a given string, by removing non-Latin (emoji) characters,
     * then removing leading and trailing whitespace, and then changing all the letters to uppercase
     * to see if there is a Move that matches.
     *
     * @param moveString the string to parse
     * @return the matching Move
     */
    public static Move parse(String moveString) {
        return valueOf(moveString.replaceAll("\\P{InBasic_Latin}", "").trim().toUpperCase());
    }

    /**
     * Poll choice strings for moves show an emoji, followed by a space, followed by the title case
     * version of the move name.
     *
     * @param moves the moves to convert
     * @return the list of string representations of the moves, with emojis included
     */
    public static List<String> toPollChoices(List<Move> moves) {
        List<String> choices = new ArrayList<>();
        for (Move move : moves) {
            choices.add(move.emojiAndTitleCase());
        }
        return choices;
    }
}
