package sondow.tetra.shape;

import sondow.tetra.game.Direction;
import sondow.tetra.game.Game;

public class ShapeConfig {
    private final Game context;

    private final Direction direction;
    private final int rowIndex;
    private final int columnIndex;

    ShapeConfig(Game context, Direction direction, int rowIndex, int columnIndex) {
        this.context = context;
        this.direction = direction;
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
    }

    Game getContext() {
        return context;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public int getColumnIndex() {
        return columnIndex;
    }
}
