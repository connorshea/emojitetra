package sondow.tetra.shape;

/**
 * This exception gets thrown when requesting a ShapeType for a character or string that does not
 * have a match a known ShapeType.
 *
 * @author @JoeSondow
 */
public class NoSuchShapeException extends RuntimeException {

    /**
     * Creates a new <code>NoSuchShapeException</code> for a specified non-shape string.
     *
     * @param s the string for which a ShapeType was requested
     */
    NoSuchShapeException(String s) {
        super("No ShapeType for " + s);
    }
}
